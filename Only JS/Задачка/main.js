var multiplicationTableHolder = document.getElementById('multiplication-table');
var matrixHolder = document.getElementById('matrix');
var pascalTriangleHolder = document.getElementById('pascal-triangle');

matrixHolder.innerHTML =  render(matrix(10));
multiplicationTableHolder.innerHTML = render(multiplicationTable(10));
pascalTriangleHolder.innerHTML = render(pascal(10));


function multiplicationTable(size) {
  var table = [];
	for (var i = 0; i < size; i++) {
	table[i] = [];
	}
		for (var x = 0; x < size; x++){
			for (var y = 0; y < size; y++){
				var z = (x + 1) * (y + 1);
				table[x][y] = z;
			};
		};
  return table;
}

function matrix (size) {
  var matrix = [];
	for (var i = 0; i < size; i++) {
		matrix[i] = [];
		}
			for (var x = 0; x < size; x++){
				for (var y = 0; y < size; y++){
					if ((x < y) && (x < 4)) {matrix[x][y] = 3;};
					if ((x < y) && (y > 5)) {matrix[x][y] = 4;};
					if ((x > y) && (x > 5)) {matrix[x][y] = 5;};
					if ((x > y) && (y < 4)) {matrix[x][y] = 6;};
					if (x == y) {matrix[x][y] = 1;};
					if ((x + y) == 9) {matrix[x][y] = 2;};
				};
			};
   return matrix;
}


function pascal (size) {  //��� ������ ������� ��� ���������� ��� ���� �����!!!! ��� ���� ����, ��� ������� ������!!!
  var triangle = [];
  for (var i = 0; i < size; i++) {
	triangle[i] = [];
	}
		for (var x = 0; x < size; x++){
			for (var y = 0; y < size; y++){
				var c = 1;
				for (var t = 1; t <= x; t++){
				c = c*t;
				};
				
				var s = 1;
				for (var w = 1; w <= y; w++){
				s = s*w;
				};
				var d = x - y;
				
				var r = 1;
				for (var p = 1; p <= d; p++){
				r = r*p;
				};
				var q = c/s/r;
				if ((x - y) < 0) {triangle[x][y] = " ";}
				else {triangle[x][y] = q;}
			};
		};
  
  
  
  return triangle;
}

function render (array) {
  var rowsQty = array.length;
  var result = [];
  for (var i = 0; i < rowsQty; i++) {
    var row = ['<tr><td>', array[i].join('</td><td>'), '</td></tr>'].join('');
    result.push(row);
  }
  return result.join('');
}